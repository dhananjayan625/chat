import { useRef } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import { v4 as uuidV4 } from 'uuid'

export default function Login(props) {
    const { onSubmit } = props
    const idRef = useRef()

    const handleSubmit = e => {
        e.preventDefault()
        onSubmit(idRef.current.value)
    }
    const createNewId = () => {
        onSubmit(uuidV4())
    }
    return (
        <Container className='align-items-center d-flex' style={{ height: "100vh" }}>
            <Form onSubmit={handleSubmit} className='w-100'>
                <Form.Group>
                    <Form.Label>Enter your Id</Form.Label>
                    <Form.Control type="text" ref={idRef} required />
                </Form.Group>
                <div className='mt-2' style={{ display: 'flex', gap: 5 }}>

                    <Button type="submit">Login</Button>
                    <Button variant="secondary" onClick={createNewId}>Create A New Id</Button>
                </div>
            </Form>
        </Container>
    )
}
