import { Fragment, useRef, useState } from 'react'
import { Container, Form, Button, Card, Alert } from 'react-bootstrap'
import { useAuth } from '../context/AuthContext'


export default function Login(props) {
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)
    const emailRef = useRef()
    const passwordRef = useRef()
    const passwordConfirmRef = useRef()

    const { signUp } = useAuth();

    const handleSubmit = async e => {
        e.preventDefault()
        const email = emailRef.current.value
        const password = passwordRef.current.value
        const passwordConfirm = passwordConfirmRef.current.value
        console.table({email, password, passwordConfirm})
        if(password !== passwordConfirm) {
            return setError('Password do not match')
        }
        try {
            setError('')
            setLoading(true)
            await signUp(email, password)
        } catch (error) {
            console.log(error)
            setError('Failed to create an account')
        }
        setLoading(false)
        // onSubmit(idRef.current.value)
    }
    return (
        <Fragment >
            <Card>
                <Card.Body>
                    <h2 className='text-center mb-4'>Sign Up</h2>
                    {error && <Alert className='text-center mb-4'>{error}</Alert>}
                    <Form onSubmit={handleSubmit} className='w-100'>
                        <Form.Group id="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" ref={emailRef} required />
                        </Form.Group>
                        <Form.Group id="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" ref={passwordRef} required />
                        </Form.Group>
                        <Form.Group id="password-confirm">
                            <Form.Label>Password Confirmation</Form.Label>
                            <Form.Control type="password" ref={passwordConfirmRef} required />
                        </Form.Group>

                        <Button className='w-100 mt-3' disabled={loading} type="submit">Sign Up</Button>
                    </Form>
                </Card.Body>
            </Card>
            <div className='w-100 text-center mt-2'>
                Already have an account? Log in
            </div>
        </Fragment>
    )
}
