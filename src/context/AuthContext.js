import React, { useContext, useState, useEffect } from 'react'
import {auth} from '../firebase'

const AuthContext = React.createContext();

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({ children }) {

    const [currentUser, setCurrentUser] = useState()

    const signup = (email, password) => {
        auth.createUserWithEmailAndPassword(email, password)
    }

    useEffect(() => {
        const unSubscribe = auth && auth.onAuthStateChanged(user => {
            setCurrentUser(user)
        })
    
      return unSubscribe
    }, [])
    

    

    const value = {
        currentUser,
        signup
    }
    return (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    )
}
