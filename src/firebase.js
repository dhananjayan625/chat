import * as firebase from 'firebase/app'
import "firebase/auth"

const app = firebase.initializeApp({
    apiKey: process.env.CHAT_FIREBASE_API_KEY,
    authDomain: process.env.CHAT_FIREBASE_AUTH_DOMAIN,
    projectId: process.env.CHAT_FIREBASE_PROJECT_ID,
    storageBucket: process.env.CHAT_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.CHAT_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.CHAT_FIREBASE_APP_ID,
})

export const auth = app.auth && app.auth()

export default app