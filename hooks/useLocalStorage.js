import { useEffect, useState } from 'react'

const PREFIX = 'CHAT'

export default function useLocalStorage(key, initialValue) {
    const prefixKey = PREFIX + key
    const isBrowser = typeof window !== "undefined"
    console.log('asf', isBrowser)
    const [value, setValue] = useState(() => {
        const jsonValue = isBrowser ? localStorage.getItem(prefixKey) : null;
        console.log('-------------json value', jsonValue)
        console.log("jsonValue", jsonValue)
        // if (jsonValue != null) return JSON.parse(jsonValue)
        if(jsonValue != null) {
            try {
                return JSON.parse(jsonValue)
            } catch (error) {
                
            }
        }
        if (typeof initialValue === 'function') {
            return initialValue()
        } else {
            return initialValue
        }
    })

    useEffect(() => {
        localStorage.setItem(prefixKey, JSON.stringify(value))
    }, [prefixKey, value])

    return [value, setValue]

}
